﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace TestandoRotas.Testes
{
    public class StubHttpContext : HttpContextBase
    {
        StubHttpRequest _request;
        StubHttpResponse _response;

        public StubHttpContext(string appPath = "/", string requestUrl = "~/")
        {
            _request = new StubHttpRequest(appPath, requestUrl);
            _response = new StubHttpResponse();
        }

        public override HttpRequestBase Request
        {
            get { return _request; }
        }

        public override HttpResponseBase Response
        {
            get { return _response; }
        }
    }

    public class StubHttpRequest : HttpRequestBase
    {
        string _appPath;
        string _requestUrl;

        public StubHttpRequest(string appPath, string requestUrl)
        {
            _appPath = appPath;
            _requestUrl = requestUrl;
        }

        public override string ApplicationPath
        {
            get { return _appPath; }
        }

        public override string AppRelativeCurrentExecutionFilePath
        {
            get { return _requestUrl; }
        }

        public override string PathInfo
        {
            get { return ""; }
        }

        public override NameValueCollection ServerVariables
        {
            get { return new NameValueCollection(); }
        }
    }

    public class StubHttpResponse : HttpResponseBase
    {
        public override string ApplyAppPathModifier(string virtualPath)
        {
            return virtualPath;
        }
    }
}
