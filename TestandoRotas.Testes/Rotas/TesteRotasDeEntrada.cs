﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace TestandoRotas.Testes.Rotas
{
    [TestFixture]
    public class TesteRotasDeEntrada
    {
        [Test]
        public void DetalheDePost()
        {
            "~/Post/sobre-tdd".Match(controller: "Posts", action: "Detalhe", routeValues: new { titulo = "sobre-tdd" });
        }

        [Test]
        public void PaginandoPosts()
        {
            "~/Posts".Match(controller: "Posts", action: "Index", routeValues: new { page = "1" });
            "~/Posts/2".Match(controller: "Posts", action: "Index", routeValues: new { page = "2" });
        }

        [Test]
        public void PaginandoPostsCategorizado()
        {
            "~/Posts/MVC".Match(controller: "Posts", action: "Categorizado", routeValues: new { categoria = "MVC", page = "1" });
            "~/Posts/MVC/2".Match(controller: "Posts", action: "Categorizado", routeValues: new { categoria = "MVC", page = "2" });
        }
    }
}
