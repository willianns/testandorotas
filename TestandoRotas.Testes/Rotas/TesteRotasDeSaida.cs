﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace TestandoRotas.Testes.Rotas
{
    [TestFixture]
    public class TesteRotasDeSaida
    {
        [Test]
        public void PaginandoPosts()
        {
            "/".MatchRouteUrlFor(new { controller = "Posts", action = "Index", page = "1" });
            "/Posts/2".MatchRouteUrlFor(new { controller = "Posts", action = "Index", page = "2" });
        }

        [Test]
        public void LinkParaPost()
        {
            "/Post/sobre-tdd".MatchRouteUrlFor(new { controller = "Posts", action = "Detalhe", titulo = "sobre-tdd" });
        }

        [Test]
        public void PaginandoPostsPorCategoria()
        {
            "/Posts/MVC".MatchRouteUrlFor(new { controller = "Posts", action = "Categorizado", categoria = "MVC", page = "1" });
            "/Posts/MVC/25".MatchRouteUrlFor(new { controller = "Posts", action = "Categorizado", categoria = "MVC", page = "25" });
        }
    }
}
