﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using NUnit.Framework;

namespace TestandoRotas.Testes
{
    public static class Helpers
    {
        public static void Match(this string url, string controller = "Home", string action = "Index", object routeValues = null)
        {
            var httpcontext = new StubHttpContext(requestUrl: url);
            var routes = new RouteCollection();
            MvcApplication.RegisterRoutes(routes);
            RouteData routeData = routes.GetRouteData(httpcontext);

            var _matchRouteValues = true;
            var _routeFailValue = "";
            if (routeValues != null)
            {
                PropertyInfo[] props = routeValues.GetType().GetProperties();

                foreach (var prop in props)
                {
                    if (!prop.GetValue(routeValues, null).Equals(routeData.Values[prop.Name]))
                    {
                        _matchRouteValues = false;
                        _routeFailValue = prop.Name;
                        break;
                    }
                }
            }

            Assert.IsNotNull(routeData, "routeData nulo, url não foi resolvida corretamente");
            Assert.AreEqual(controller, routeData.Values["controller"]);
            Assert.AreEqual(action, routeData.Values["action"]);
            Assert.IsTrue(_matchRouteValues, String.Format("route value '{0}' not match!", _routeFailValue));
        }

        public static void MatchRouteUrlFor(this string result, object routeValues = null)
        {
            var helper = Helpers.UrlHelper();
            string url = helper.RouteUrl(routeValues);

            Assert.AreEqual(result, url);
        }

        static UrlHelper UrlHelper(RouteCollection routes = null)
        {
            if (routes == null)
            {
                routes = new RouteCollection();
                MvcApplication.RegisterRoutes(routes);
            }

            StubHttpContext httpContext = new StubHttpContext();
            RequestContext requestContext = new RequestContext(httpContext, new RouteData());
            UrlHelper helper = new UrlHelper(requestContext, routes);
            return helper;
        }
    }
}
